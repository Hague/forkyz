
package app.crossword.yourealwaysbe.forkyz.settings

public class FileHandlerSettings(
    val storageLocation : StorageLocation,
    val safRootURI : String,
    val safCrosswordsURI : String,
    val safArchiveURI : String,
    val safToImportURI : String,
    val safToImportDoneURI : String,
    val safToImportFailedURI : String,
)

