
package app.crossword.yourealwaysbe.forkyz.settings

class RatingsSettings(
    val disableRatings : Boolean,
    val browseAlwaysShowRating : Boolean,
)
