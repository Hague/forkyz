
package app.crossword.yourealwaysbe.forkyz.settings

class RenderSettings(
    val displayScratch : Boolean,
    val suppressHintHighlighting : Boolean,
    val displaySeparators : DisplaySeparators,
    val inferSeparators : Boolean,
)
