
package app.crossword.yourealwaysbe.forkyz.theme

import androidx.compose.ui.graphics.Color

///////////////////////////////////////////////////////////
// copy of values/color.xml

val primaryLight = Color(0xff425578)
val onPrimaryLight = Color(0xffffffff)
val primaryContainerLight = Color(0xffcee5ff)
val onPrimaryContainerLight = Color(0xff262626)
val secondaryLight = primaryLight
val onSecondaryLight = onPrimaryLight
val secondaryContainerLight = primaryContainerLight
val onSecondaryContainerLight = onPrimaryContainerLight
val tertiaryLight = secondaryLight
val onTertiaryLight = onSecondaryLight
val tertiaryContainerLight = secondaryContainerLight
val onTertiaryContainerLight = onSecondaryContainerLight
val errorLight = Color(0xffBA1A1A)
val errorContainerLight = Color(0xffFFDAD6)
val onErrorLight = Color(0xffFFFFFF)
val onErrorContainerLight = Color(0xff410002)
val backgroundLight = Color(0xffeaeaea)
val onBackgroundLight = Color(0xff262626)
val onBackgroundSecondaryLight = Color(0xff565656)
val surfaceLight = backgroundLight
val onSurfaceLight = onBackgroundLight
val surfaceVariantLight = Color(0xfffafafa)
val onSurfaceVariantLight = Color(0xff262626)
val outlineLight = Color(0xff72777F)
val inverseOnSurfaceLight = Color(0xffd9d9d9)
val inverseSurfaceLight = Color(0xff303030)
val inversePrimaryLight = Color(0xffbdaa87)
val surfaceTintLight = primaryLight

val surfaceContainerLowestLight = Color(0xffeaeaea)
val surfaceContainerLowLight = Color(0xffe1e2e4)
val surfaceContainerLight = Color(0xffdedfe1)
val surfaceContainerHighLight = Color(0xffdcdde0)
val surfaceContainerHighestLight = Color(0xffdadbdf)

val currentWordHighlightColorLight = Color(0xffFFAE57)
val currentLetterHighlightColorLight = Color(0xffEB6000)
val cheatedColorLight = Color(0xffFFE0E0)
val boardLetterColorLight = Color(0xff000000)
val boardNoteColorLight = Color(0x60000000)
val boardShapeColorLight = Color(0xffaaaaaa)
val blockShapeColorLight = Color(0xff555555)
val blockColorLight = Color(0xff000000)
val onBlockColorLight = Color(0xffffffff)
val errorColorLight = Color(0xffff0000)
val errorHighlightColorLight = Color(0xffff0000)
val cellColorLight = Color(0xffffffff)
val flagColorLight = errorColorLight

val screenFadeLight = Color(0x66eaeaea)

val progressNullLight = Color(0xffb4b4b4)
val progressInProgressLight = Color(0xffd5a518)
val progressDoneLight = Color(0xff31915a)
val progressErrorLight = Color(0xffff4a4d)

// for legacy-like theme
val primaryDarkLight = Color(0xff334465)
val speedDialTintLight = Color(0xff669900)
val speedDialIconTintLight = Color(0xffffffff)
val legacySurfaceContainerLowestLight = Color(0xffeaeaea)
val legacySurfaceContainerLowLight = Color(0xffeaeaea)
val legacySurfaceContainerLight = Color(0xffeaeaea)
val legacySurfaceContainerHighLight = Color(0xffeaeaea)
val legacySurfaceContainerHighestLight = Color(0xffeaeaea)

///////////////////////////////////////////////////////////
// copy of values-night/color.xml

val primaryDark = Color(0xffad5620)
val onPrimaryDark = Color(0xffdddddd)
val primaryContainerDark = Color(0xff4d3020)
val onPrimaryContainerDark = Color(0xffdddddd)
val secondaryDark = primaryDark
val onSecondaryDark = onPrimaryDark
val secondaryContainerDark = primaryContainerDark
val onSecondaryContainerDark = onPrimaryContainerDark
val tertiaryDark = secondaryDark
val onTertiaryDark = onSecondaryDark
val tertiaryContainerDark = secondaryContainerDark
val onTertiaryContainerDark = onSecondaryContainerDark
val errorDark = Color(0xffFFB4AB)
val errorContainerDark = Color(0xff93000A)
val onErrorDark = Color(0xff690005)
val onErrorContainerDark = Color(0xffFFDAD6)
val backgroundDark = Color(0xff222222)
val onBackgroundDark = Color(0xffbbbbbb)
val onBackgroundSecondaryDark = Color(0xff888888)
val surfaceDark = backgroundDark
val onSurfaceDark = onBackgroundDark
val surfaceVariantDark = Color(0xff333333)
val onSurfaceVariantDark = Color(0xffbbbbbb)
val outlineDark = Color(0xff8C9199)
val inverseOnSurfaceDark = Color(0xff444444)
val inverseSurfaceDark = Color(0xffdddddd)
val inversePrimaryDark = Color(0xff52a9df)
val surfaceTintDark = Color(0xff999999)

val surfaceContainerLowestDark = Color(0xff222222)
val surfaceContainerLowDark = Color(0xff282828)
val surfaceContainerDark = Color(0xff2a2a2a)
val surfaceContainerHighDark = Color(0xff2b2b2b)
val surfaceContainerHighestDark = Color(0xff2d2d2d)

val cellColorDark = Color(0xff333333)
val blockColorDark = Color(0xff000000)
val onBlockColorDark = Color(0xffaaaaaa)
val errorColorDark = Color(0xff04526c)
val errorHighlightColorDark = Color(0xff09b7ef)
val currentWordHighlightColorDark = Color(0xff743513)
val currentLetterHighlightColorDark = Color(0xffad5620)
val cheatedColorDark = Color(0xff553333)
val boardLetterColorDark = Color(0xffaaaaaa)
val boardNoteColorDark = Color(0x80aaaaaa)
val boardShapeColorDark = Color(0xff555555)
val blockShapeColorDark = Color(0xff444444)
val flagColorDark = Color(0xff34bee7)

val screenFadeDark = Color(0x77333333)

val progressNullDark = Color(0xffb4b4b4)
val progressInProgressDark = Color(0xffd5a518)
val progressDoneDark = Color(0xff31915a)
val progressErrorDark = Color(0xffff4a4d)

// for legacy-like theme
val primaryDarkDark = Color(0xff000000)
// not used in night mode -->
val speedDialTintDark = Color(0xff000000)
val speedDialIconTintDark = Color(0xff000000)

