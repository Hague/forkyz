
package app.crossword.yourealwaysbe.forkyz.util

import java.io.IOException
import java.util.function.Consumer
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.CoroutineScope

import androidx.annotation.MainThread
import androidx.core.app.ServiceCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import android.content.Context
import android.content.Intent
import android.app.Service
import android.os.IBinder

import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext

import app.crossword.yourealwaysbe.forkyz.R
import app.crossword.yourealwaysbe.forkyz.settings.ForkyzSettings
import app.crossword.yourealwaysbe.forkyz.util.files.FileHandlerProvider
import app.crossword.yourealwaysbe.forkyz.util.files.PuzHandle
import app.crossword.yourealwaysbe.forkyz.util.files.PuzMetaFile
import app.crossword.yourealwaysbe.forkyz.versions.AndroidVersionUtils
import app.crossword.yourealwaysbe.puz.Playboard

/**
 * Holder for the current puzzle being played
 *
 * Singleton and app-wide. Persists until application destroyed
 * (property of Hilt singletons). Can be Hilt-injected into whichever
 * class needs access to get/set the current puzzle.
 */
@Singleton
public class CurrentPuzzleHolder @Inject constructor(
    @ApplicationContext val appContext : Context,
    val settings : ForkyzSettings,
    val fileHandlerProvider : FileHandlerProvider,
) {
    private val LOG = Logger.getLogger(
        CurrentPuzzleHolder::class.toString()
    )

    var board : Playboard? = null
        private set
    var puzHandle : PuzHandle? = null
        private set

    // even though using a mutex need one thread to ensure mutex lock
    // in order of request
    @OptIn(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
    private val ioScope = CoroutineScope(Dispatchers.IO.limitedParallelism(1))
    private val mainScope = CoroutineScope(Dispatchers.Main)
    private val ioLock = Mutex()

    init { setupObservers() }

    /**
     * Load puzzle and set as current board
     *
     * Delegates to io and calls back on main thread when done.
     */
    @MainThread
    fun loadPuzzle(
        puzMeta : PuzMetaFile,
        cb : Runnable,
        onErr : Consumer<IOException>,
    ) {
        ioScope.launch {
            ioLock.lock()
            // main scope needed because currently settings can only get
            // on main thread (because they return on main thread)
            mainScope.launch {
                settings.getPlayPreserveCorrectLettersInShowErrors() {
                    preserveCorrect ->
                settings.getPlayDeleteCrossingMode() { deleteCrossingMode ->
                settings.getPlayShowErrorsGrid() { showErrorsGrid ->
                settings.getPlayShowErrorsCursor() { showErrorsCursor ->
                settings.getPlayShowErrorsClue() { showErrorsClue ->
                settings.getPlayToggleBeforeMove() { toggleBeforeMove ->
                settings.getPlayMovementStrategy() { movementStrategy ->
                settings.getPlaySkipFilled() { skipFilled ->
                settings.getPlayPlayLetterUndoEnabled() { undoEnabled ->
                fileHandlerProvider.get() { fileHandler ->
                    // on main thread so go back to io
                    ioScope.launch {
                        try {
                            val puz = fileHandler.load(puzMeta);
                            if (puz == null || puz.getBoxes() == null) {
                                throw IOException(
                                    "Puzzle is null or contains no boxes."
                                )
                            }
                            mainScope.launch (Dispatchers.Main) {
                                board = Playboard(
                                    puz,
                                    movementStrategy,
                                    preserveCorrect,
                                    deleteCrossingMode,
                                    showErrorsCursor,
                                    showErrorsClue,
                                    showErrorsGrid,
                                    toggleBeforeMove,
                                    skipFilled,
                                    undoEnabled
                                )
                                puzHandle = puzMeta.getPuzHandle()
                                cb.run()
                            }
                        } catch (e : IOException) {
                            mainScope.launch { onErr.accept(e) }
                        } finally {
                            ioLock.unlock()
                        }
                    }
                }}}}}}}}}}
            }
        }
    }

    /**
     * Save current board
     *
     * Delegates to io thread
     */
    @MainThread
    fun saveBoard() {
        ContextCompat.startForegroundService(
            appContext,
            Intent(appContext, PuzzleSaveService::class.java),
        )
    }

    /**
     * Get/refresh meta of current puzzle
     *
     * Delegates to io thread and calls back on main. Does nothing if
     * nothing loaded. If error, calls back with null.
     */
    @MainThread
    fun getFreshCurrentPuzzleMeta(cb : Consumer<PuzMetaFile?>) {
        if (puzHandle == null)
            return

        ioScope.launch {
            ioLock.lock()
            mainScope.launch {
                fileHandlerProvider.get() { fileHandler ->
                    ioScope.launch {
                        var meta : PuzMetaFile? = null
                        try {
                            meta = fileHandler.loadPuzMetaFile(puzHandle)
                        } catch (e : IOException) {
                            LOG.severe(
                                "Could not get current puzzle meta: " + e,
                            )
                        } finally {
                            ioLock.unlock()
                            mainScope.launch {
                                cb.accept(meta)
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Save board from save service
     *
     * App should use saveBoard()
     */
    fun saveBoardService(cb : () -> Unit) {
        val puz = board?.getPuzzle()
        if (puz == null) {
            LOG.severe("No puzzle associated to the board to save.")
            cb()
        }

        ioScope.launch {
            ioLock.lock()
            mainScope.launch {
                fileHandlerProvider.get() { fileHandler ->
                    ioScope.launch {
                        try {
                            fileHandler.save(puz, puzHandle)
                        } catch (e : IOException) {
                            LOG.severe("Error saving puzzle.")
                            e.printStackTrace()
                        } finally {
                            ioLock.unlock()
                            cb()
                        }
                    }
                }
            }
        }
    }

    private fun setupObservers() {
        // these keep the current board up to date with changes in
        // settings -- rather than expecting activities to do it on
        // resume.
        // Ok to observe forever because this is a singleton that lives
        // forever.
        settings.livePlayPreserveCorrectLettersInShowErrors.observeForever() {
            board?.setPreserveCorrectLettersInShowErrors(it)
        }
        settings.livePlayDeleteCrossingMode.observeForever() {
            board?.setDeleteCrossingMode(it)
        }
        settings.livePlayShowErrorsGrid.observeForever() {
            board?.setShowErrorsGrid(it)
        }
        settings.livePlayShowErrorsCursor.observeForever() {
            board?.setShowErrorsCursor(it)
        }
        settings.livePlayShowErrorsClue.observeForever() {
            board?.setShowErrorsClue(it)
        }
        settings.livePlayToggleBeforeMove.observeForever() {
            board?.setToggleBeforeMove(it)
        }
        settings.livePlayMovementStrategy.observeForever() {
            board?.setMovementStrategy(it)
        }
        settings.livePlaySkipFilled.observeForever {
            board?.setSkipCompletedLetters(it)
        }
        settings.livePlayPlayLetterUndoEnabled.observeForever {
            board?.setPlayLetterUndoStackEnabled(it)
        }
    }
}

@AndroidEntryPoint
class PuzzleSaveService() : Service() {
    companion object {
        @JvmField
        final val NOTIFICATION_CHANNEL_ID = "forkyz.save"
        private final val NOTIFICATION_ID = 10102
    }

    @Inject
    @ApplicationContext
    lateinit var context : Context

    @Inject
    lateinit var currentPuzzleHolder : CurrentPuzzleHolder

    @Inject
    lateinit var utils : AndroidVersionUtils

    override fun onBind(intent : Intent): IBinder? {
        return null
    }

    override fun onStartCommand(
        intent : Intent,
        flags : Int,
        startId : Int,
    ) : Int {
        val notification = NotificationCompat.Builder(
            this,
            NOTIFICATION_CHANNEL_ID,
        ).setSmallIcon(android.R.drawable.ic_menu_save)
            .setContentTitle(context.getString(R.string.saving_puzzle))
            .setWhen(System.currentTimeMillis())
            .build()

        ServiceCompat.startForeground(
            this,
            NOTIFICATION_ID,
            notification,
            utils.foregroundServiceTypeShortService(),
        )

        currentPuzzleHolder.saveBoardService(this::stopSelf)

        return START_STICKY
    }
}

